# Dokuwiki Plugin pot

in short:

This [https://www.dokuwiki.org|Dokuwiki] plugin allows you to define “pot” in a page, which can be used anywhere in the document (page).

Mainly it is used to calculate and display formatted numbers. In addition, the content can be retrieved in the further course of the page.








<!--

## pot Plugin

---- plugin ---- 
description: calculate and display formatted numbers 
author     : agapetus 
email      :  
type       : syntax 
lastupdate : 2021-12-27 
compatible : Hogfather 
depends    :  
conflicts  :  
similar    :  
tags       :

downloadurl:  
bugtracker :  
sourcerepo :  
donationurl:

----

-->


The pot plugin is based on the  [https://www.dokuwiki.org/plugin:calc|calc plugin] and inspired by [https://www.dokuwiki.org/plugin:fields|fields plugin] It allows to perform calculations and displays numbers in an appropriate formatting.



### Syntax 

The easy way for example

    <pot> 5 + 10 </pot>


### format the result

Additional parameters can be used to adjust the displayed result:

    <pot currency=$ | decimals=2 > 5 + 10 </pot>

You will get:  15.00 $

There are three ways to customize the display of results:

  * use the config in the admin section for a general design,
  * use an page specific code to design an shortcut
  * use the individual "< pot settings=setting  > " to adjust only this "< pot>"
  * the general config settings will be merged ( overwritten ) by the page code (if set), and finally merged by the individual settings (if set).

The values are a simple line of entries in the format: "key=value | key=value | key=value

| key  | shortcut  | (default) value  | why  |
| --- | --- | --- | --- |
| set  |   | any number or word,  | with this key word you will set a page default, see below  |
| potid  | @value or #value  | unique id for this pot, see below: shortcut  | necessary if you want to use this pot-result within an other pot  |
| decimals  |   | (0) how many numbers after the decimal point  |   |
| currency  |   | () symbols ( €,$,...) or letters ( EUR, DOL,..)  |   |
|           |   | you can use _ to place the currency before or after the value: eg. "_ €" is replaced to "value €" and "€ _" is replaced to "€ value"  (default ist after)  |   |
| width  |   | (0) length of the result field in the format: 10px  | if you need all numbers nicely below the other  |
| formula  |   | (false) true  | shows the formula if true  |
| display  |   | (flex) none  | none if you need to hide the pot ( it is still shown in the html code!)  |
| float  | !  | (left) right  |   |
| sepmil  |   | (,)  | the sign which separates the thousand (1.000,00)  |
| sepdec  |   | (.)  | the sign which separates the decimals (0,00)  |
| defaultentry  | ~value  |   | shortcut, if you have used "set=3" you can use ~3 to call all settings done within the pot with set=3  |
| color+  |   |   | set a color for results above 0  |
| color-  |   |   | set a color for results beneath 0  |
| type  |   | (formula) text  | the pot can be used to calculate formulas or to set and reuse text  |



#### more examples 

    <pot set=1 | potid=example | decimals=2 | currency=€ | sepdec=, | sepmil=. | display=none > 1100.01 </pot>

Will not be displayed. ( display=none )

    <pot ~1> {@example} + 1  </pot>

Will be displayed as:  1.101,01 € ( because  of the formatting set with set=1 )



#### calculating

Besides the simple calculations ( + - * / ) more complex functions can be used (e.g. sin() cos() ...).

Already set "< pot>" values can be retrieved and included in the formulas. To retrieve exactly one result write "{@potid}".

    <pot> {@example} + 1 </pot> to fetch exactly one other pot result

You also can also write "{#examp}" to get the sum of all pots above this entry which have the letters "examp" in it's potid. (Keep in mind it is the sum of all above ones, the hidden ones and the ones with automatically set potid as well.)

    <pot> {#examp} </pot>                                 to sum up all pots where id "examp" occurs
  
    <pot potid=example_5 > {&} </pot>                     to sum up all pots where this pod id ("example" without added numbers) occurs

The calculation might be done with the result of former pots and not with the formulas ( have a look at "formula=true" to see more), this might give different results (e.g. because of the amount of decimals used ).



#### potid

With "potid" you can set individual names to each pot. If not, it will be set automatically. There is a simple rule in the automatically set name, it ads an rising number to the last potid set. Simply set "potid=example5" and the next pots will have: example6, example7, example8,... until you set a new name where the rising starts again.



#### shortcuts

Some signs are definde as shortcuts. You might use them in the "format" part.

    <pot @example_5 > 1 </pot>                           is identical to  <pot potid=example_5 > 1 </pot>

| shortcut | the same as |   |
|---------| ------------| ----- |
| @value  | potid=value |   |
| #value | potid=value_(uniqueNumber) | nice to use in tables and to sum them with {#value} later |
| ! | float= right | probebly best in tables to have your numbers nicely on the right side |
| ~value | defaultentry=value | fetches page settings which were set erlier with "set=value" |



#### text 

To use this plugin for text entries simply write:

    <POT> my text </POT>         or      <pot format=text > my text </pot>

You can use this text later on the page with {@value}. A other plugin with similar functions: [https://www.dokuwiki.org/plugin:fields|plugin fields].



#### WYSiWYG editor

Unfortunately, error messages occur when using [https://www.dokuwiki.org/plugin:prosemirror|plugin:prosemirror] and  [https://www.dokuwiki.org/plugin:edittable|plugin:edittable]. The error messages occur when preview is clicked in the editor. After regular saving of the page the display is as usual.



#### SAFETY CONSIDERATIONS

In the current development state, this plugin should not be used in productive environments or open wikis.



#### PHP Javascript

This first attempt is only done with php. That is why the calculations and name assignments are made from top to bottom. Possibly, at some point, a variant with javascript will be created in order to be able to retrieve the set values on the whole page unhindered.

#### developer notes

Unfortunately, I do not have the necessary time or knowledge to maintain this plugin permanently or to improve it significantly. Please feel free to use, copy and fork the plugin.

