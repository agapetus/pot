<?php

/* format the output */

/*go on with the given value from <pot ? >*/
$formatedsettings = $pothelper->_formatsettings($potsettings);

/*check if we have to set pagedefault and set it to cargo*/
if (isset($formatedsettings['set'])) {
	$none = preg_replace('/\s*display\s*=\s*none\s*/', 'display=inline', $formatedsettings['pagedefault']); //replace var and field to avoid circles ?
	$this->cargo['settings'][$formatedsettings['set']] = $none;
}

/*check if we have a shortcut set, use the pagedefault needed */
$defsettings = array();
if (isset($formatedsettings['defaultsettings']) && $formatedsettings['defaultsettings'] != '') {
	$def = $this->cargo['settings'][$formatedsettings['defaultsettings']];
	$defsettings = $pothelper->_formatsettings($def);
}

/* start with some default */
/* last we create the array from the config default */
$configdefault = $this->getConf('default_settings');
$configdefault = $pothelper->_formatsettings($configdefault);
//$backupdefault = "potid=pot_1 | decimals=2 | currency= | color+='' | color-=''| width=0 | formula=false | display=flex | sepmil=. | sepdec=, | float=left | type=formula ";
$backupdefault = $this->getConf('hidden__settings');  //fetches the hidden config elements, 
$backupdefault = $pothelper->_formatsettings($backupdefault);
$defaultsettings = array_merge($backupdefault, $configdefault);


/*set potid,check if it is unique, set it from input or ad a new number at the end */
$forpotid = isset($formatedsettings['potid']) ? $formatedsettings['potid'] : '' ;
//print("<pre> aha: ".print_r($formatedsettings, true)."</pre>");
//print("<pre> pot".print_r($this->mycargo['pot'], true)."</pre>");

if (array_key_exists('potid', $formatedsettings)) {
	if (array_key_exists($forpotid, $this->cargo)) {
		$potid = $forpotid."_".uniqid();
		$this->cargo[$potid]['output']['error'] = "potid already exists: $forpotid ";
		
	} else {
		$potid = $formatedsettings['potid'];
	}
} else {
$potid = "leer_".uniqid();
}

/* merge general default and pagedefault and potsettings */
$finalsettings = array_merge($defaultsettings, $defsettings, $formatedsettings);

$this->cargo['pot'] = $potid; // set the var to use it global
$finalsettings['potid'] = $potid; // yes this value is double , why?
$this->cargo[$potid]['settings'] = $finalsettings; // set the var to use it global