<?php
/**
 * Action Component for the POT Plugin
 */

/*
// must be run within Dokuwiki
if(!defined('DOKU_INC')) die();

if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'action.php');
*/
use dokuwiki\Extension\ActionPlugin;
use dokuwiki\Extension\EventHandler;
use dokuwiki\Extension\Event;
 



class action_plugin_pot extends DokuWiki_Action_Plugin {

    /**
     * register the eventhandlers
     */
    function register(Doku_Event_Handler $controller){
        $controller->register_hook('TOOLBAR_DEFINE', 'AFTER', $this, 'handle_toolbar', array ());
    }

    function handle_toolbar(Doku_Event $event, $param) {

        $event->data[] = array (
            'type' => 'picker',
            'title' => $this->getLang('picker'),
            'icon' => '../../plugins/pot/images/pot.png',
            'list' => array(
                array(
                    'type'   => 'format',
                    'title'  => $this->getLang('formula'),
                    'icon'   => '../../plugins/pot/images/pot_formula.png',
                    'open'   => '<pot>',
                    'close'  => '</pot>',
                    'sample'  => '1',
                ),
                array(
                    'type'   => 'format',
                    'title'  => $this->getLang('text'),
                    'icon'   => '../../plugins/pot/images/pot_text.png',
                    'open'   => '<POT>',
                    'close'  => '</POT>',
                    'sample'  => 'my text',
                ),
                array(
                    'type'   => 'format',
                    'title'  => $this->getLang('table'),
                    'icon'   => '../../plugins/pot/images/pot_table.png',
                    'open'   => '<pot #table ! >',
                    'close'  => '</pot>',
                    'sample'  => '1000.00 * -2',
                ),
                array(
                    'type'   => 'format',
                    'title'  => $this->getLang('hidden'),
                    'icon'   => '../../plugins/pot/images/pot_hidden.png',
                    'open'   => '<pot display=none>',
                    'close'  => '</pot>',
                    'sample'  => '1',
                ),
                array(
                    'type'   => 'format',
                    'title'  => $this->getLang('set'),
                    'icon'   => '../../plugins/pot/images/pot_set.png',
                    'open'   => '<pot set=1 | potid=set | display=none | decimals=2 | currency=€ | width=100px | color-=black | color-=black  | sepdec=, | sepmil=. | formula=false>',
                    'close'  => '</pot>',
                    'sample'  => 'hidden',
                ),
            )
        );
    }
}
