<?php

$lang['picker']    = 'pot Plugin';
$lang['formula']   = 'pot für Zahlen und Formeln';
$lang['text']      = 'POT für Texte';
$lang['table']     = 'pot reduziert für Tabellen';
$lang['hidden']    = 'pot mit display:none';
$lang['set']       = 'pot mit set für Seiten-default';