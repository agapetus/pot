<?php
/**
* pot Plugin: Re-usable user pot
*
* @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
* @author
*/
// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN', DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

class syntax_plugin_pot extends DokuWiki_Syntax_Plugin {

	/**
	* What kind of syntax are we?
	*/
	function getType() {
		return 'formatting';
	}

	/**
	* Where to sort in?
	*/
	function getSort() {
		return 499;
	}
	/*
     * Paragraph Type
     */
	function getPType() {
		return 'normal';
	}

	function getAllowedTypes() {
		return array(
			'disabled'
		);
	}

	/**
	* Connect pattern to lexer
	*/
	public function connectTo($mode) {
		$this->Lexer->addEntryPattern('<[P|p][O|o][T|t]\b *[^>]*>(?=.*?<\/[P|p][O|o][T|t]>)', $mode, 'plugin_pot');
	//	$this->Lexer->addPattern('.*','plugin_pot');
	}

	public function postConnect() {
		$this->Lexer->addExitPattern('<\/[P|p][O|o][T|t]>', 'plugin_pot');
	}

	private $cargo = array(); //set class property to set var https://forum.dokuwiki.org/d/14219-persistent-tag-attributes-solved

	/**
	* Handle the match
	*/
	var $pothelper = null; // is this needed to prevent php8 errors on null ?

	public function handle($match, $state, $pos, Doku_Handler $handler) {
		global $ID;
		// pot helper
		$pothelper = plugin_load('helper', 'pot');

		switch ($state) {
			case DOKU_LEXER_ENTER :
				$try = $match;
				// lets try, and do some changes inside the unmatched input
				$try = preg_replace('/(\<POT\b\s*)(.*?)\s*?(\>)/', '$1 $2 | type=text $3', $try); // if we have "<POT ... >" we ad a | type=text
				$try = preg_replace('/~\s*([a-zA-Z0-9]+)\s*|\|/', '|defaultsettings=$1|', $try); // replace a '@value' to '|defaultsettings=value|'
				$try = preg_replace('/!/', '|float=right|', $try); // replace a '!' to '|float=right|'
				$try = preg_replace('/#\s*([a-zA-Z0-9]+)(\s*|\|)/', '|potid=$1_'.uniqid().'|', $try); // replace a '#value' to '|potid=value_stamp|'
				$try = preg_replace('/@\s*([a-zA-Z0-9]+)(\s*|\|)/', '|potid=$1|', $try); // replace a '@value' to '|potid=value|'

				preg_match('/\<[P|p][O|o][T|t]\b\s*(.*?)\s*?\>/', $try, $potset); // get the info
				$potsettings = $potset[1];
				//print("<pre>potsettings:  ".print_r($potsettings, true)."</pre>");
				include 'source/format.php'; // the long staff is written in an other file

				break;
			case DOKU_LEXER_MATCHED :
				break;
			case DOKU_LEXER_UNMATCHED :
				$potid = $this->cargo['pot'];
				$match = (isset($match) ?  $match : 'empty'); // if $match is not set / empty create it,   //-------- does not work! 
				
			//	print("<pre>$potid    unmatch:  ".print_r($match, true)."</pre>");
				if (trim($this->cargo[$potid]['settings']['type']) == 'text') {
					include 'source/text.php';
				} else {
					include 'source/calculate.php';
				}
				break;
			case DOKU_LEXER_EXIT :
				break;
			case DOKU_LEXER_SPECIAL :
				break;
		}
		$mycargo = $this->cargo; //fetch global var

		//return array($state, $pot, $mycargo);
		return array($state, $mycargo);
	}

	/**
	* Create output
	*/
	public function render($mode, Doku_Renderer $renderer, $data) {
		if ($mode !== 'xhtml') {
			return false;
		}

		global $INFO;
		$renderer->info['cache'] = false;
		//list($state, $pot, $mycargo) = $data;
		list($state, $mycargo) = $data;

		switch ($state) {
			case DOKU_LEXER_ENTER :
				$potid = $mycargo['pot'];
				$float = $mycargo[$potid]['settings']['float'];
				$display = $mycargo[$potid]['settings']['display'];

				$result = isset($mycargo[$potid]['output']['result']);
				if ($result < 0) {
					$color = $mycargo[$potid]['settings']['color-'];
				} else {
					$color = $mycargo[$potid]['settings']['color+'];
				}
				$renderer->doc .= "<span class='pot' id='pot_$potid'  style='$display $float $color'>";
				break;
			case DOKU_LEXER_MATCHED :
				break;
			case DOKU_LEXER_UNMATCHED :
				$potid = $mycargo['pot'];
				$width = $mycargo[$potid]['settings']['width'];
				$decimals = $mycargo[$potid]['settings']['decimals'];
				$sepdec = $mycargo[$potid]['settings']['sepdec'];
				$sepmil = $mycargo[$potid]['settings']['sepmil'];
				$display = '';
				$formulaset = $mycargo[$potid]['settings']['formula'];
				$formula = $mycargo[$potid]['output']['formula'];
				$result = $mycargo[$potid]['output']['result'];
				$wherecurrency = strpos($mycargo[$potid]['settings']['currency'], '_');
				$currency = preg_replace('/_/', '', $mycargo[$potid]['settings']['currency']);
				$type = $mycargo[$potid]['output']['type'];
				if ($type == 'formula') {
					$result = number_format($result, $decimals, $sepdec, $sepmil);
				} else {
					$result = $result;
				}
				$formula = $mycargo[$potid]['output']['formula'];
				if ($wherecurrency == 0) {
					$renderer->doc .= "<span class='pot_currency' >".$currency."&nbsp;</span>"; //currency before result
				}
				if ($formulaset == 'true') {
					$renderer->doc .= $formula."&nbsp;=&nbsp;";
					$renderer->doc .= "<span class='pot_result' id='$potid' style='$width text-align:right; '>";
					$renderer->doc .= $result;
					$renderer->doc .= "</span>";
				} else {
					$renderer->doc .= "<span class='pot_result' id='$potid' style='$width  text-align:right; '>";
					$renderer->doc .= $result;
					$renderer->doc .= "</span>";
				}
				if ($wherecurrency != 0) {
					$renderer->doc .= "<span class='pot_currency' >&nbsp;".$currency."</span>"; //currency after result
				}
				if ($mycargo[$potid]['output']['error'] != '' ) {
					$error = $mycargo[$potid]['output']['error'];
					$renderer->doc .= "<span class='pot_error style='$display'>&nbsp; <b><i> $error &nbsp; </i></b> $formula</span>";
				}
				break;
			case DOKU_LEXER_EXIT :
				$renderer->doc .= "</span>";
				break;
			case DOKU_LEXER_SPECIAL :
				break;
		}
		return true;
	}
}