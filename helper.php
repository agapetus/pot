<?php

class helper_plugin_pot extends DokuWiki_Plugin {

	// Original code from plugin:color
	// validate color value $c
	// this is cut price validation - only to ensure the basic format is
	// correct and there is nothing harmful
	// three basic formats  "colorname", "#fff[fff]", "rgb(255[%],255[%],255[%])"


	function _isValidcolor($c) {

		$c = trim($c);

		$pattern = "/
            (^[a-zA-Z]+$)|                                #colorname - not verified
            (^\#([0-9a-fA-F]{3}|[0-9a-fA-F]{6})$)|        #colorvalue
            (^rgb\(([0-9]{1,3}%?,){2}[0-9]{1,3}%?\)$)     #rgb triplet
            /x";
		//print("<pre>".print_r($c, true)."</pre>");
		preg_match($pattern, $c, $out);
		//		return (preg_match($pattern, $c));

		if (isset($out[0])) {
			return $out[0];
		}
	}


	/**
	* format settings
	* we expect something like: " key=value | key2=value2 | key3=value3 "
	* and format it to an array[key][value]
	*/
	function _formatsettings($potsettings) {
		$pureset = $potsettings;
		$potsettings = explode('|', $potsettings); // splitt template name and arguments
		$potsettings = array_map('trim', $potsettings); //remove whitespaces, linebreaks
		$potsettings = array_filter($potsettings); //remove empty array entries
		$formatedsettings = array();

		foreach ($potsettings as $potsets) {
			$temp = explode('=', $potsets);
			$key = trim($temp[0]);
			$value = trim($temp[1]) ?? '';

			// check some of the key=value to fit the value the way we like
			switch ($key) {
				case 'potid':
					$formatedsettings[$key] = $value;
					break;
				case 'color+':
					if ($value != '') {
						$formatedsettings[$key] = "color:".$this->_isValidcolor($value).";";
					} else {
						$value = "";
					}
					break;
				case 'color-':
					if ($value != '') {
						$formatedsettings[$key] = "color:".$this->_isValidcolor($value).";";
					} else {
						$value = "";
					}
					break;
				case 'display': // only limitet styles are allowed
					if ($value == 'none') {
						$value = 'display:none;';
					} else {
						//$value = 'display:inline-block;';
						$value = 'display:flex;';
					}
					$formatedsettings[$key] = $value;
					break;
				case 'float': // only limitet styles are allowed
					if ($value == 'right') {
						$value = 'float:right;';
					} else {
						//$value = 'display:inline-block;';
						$value = 'float:left;';
					}
					$formatedsettings[$key] = $value;
					break;
				case 'width': // TODO check value
					if ($value == '' || $value == 0) {
						$value = "";
						if (isset($formatedsettings['display']) && $formatedsettings['display'] != 'none') {
							$formatedsettings['display'] = ""; //no "width="" forces other no "display="
						} else {
							$value = "width:$value;";
						}
					}
					$formatedsettings[$key] = $value;
					break;
				case 'set':
					$formatedsettings[$key] = $value;
					$formatedsettings['pagedefault'] = preg_replace('/set|field/', 'var$0', $pureset); //replace var and field to avoid circles ?
					break;
				case 'defaultsettings':
					$formatedsettings[$key] = $value;
					break;

				default:
					$formatedsettings[$key] = $value;
					break;
			}
		}
		return $formatedsettings;
	}


/**
 * use <https://github.com/licosan/Formula1>  to proof the math formula,
 * calculating
 * we need: formula, decimals, sign for decimal-point, sign for thousend-point
 * we get: array with: result, formula, type
*/
	function formula1_calculate($formula, $mdec) {
		require_once('source/formula1.php');

		$formula = preg_replace('/\s+/', '', $formula); //remove whitespaces
		$formula = preg_replace('/\(\)/', '(empty)', $formula); //in case we have an empty (), happens in case of empty "<pot></pot>" tag
		//print("<pre>formula:   ".print_r($formula, true)."</pre>");
		$f1 = new Formula1($formula);
		if ($f1->parse_error == '') {
			$result = $f1->compute();
			$error = '';
			$type = 'formula';
		} else {
			$error = $f1->parse_error;
			//print("<pre enter:>".print_r($error, true)."</pre>");
			$result = '';
			//$error = "\"Bad value...\"";
			$type = 'text';
		}
		$calculated['error'] = $error;
		$calculated['type'] = $type;
		$calculated['result'] = $result;
		$calculated['formula'] = $formula;
		return $calculated;
	}

}